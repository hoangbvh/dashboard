$(document).ready(function(){       
    //treeview add report
	$("#addclick").click(function(e){
		$("#add_report").slideToggle('slow');
        e.stopPropagation();
	});
    $("#add_report").click(function(e){
    e.stopPropagation();
    });
    
    $(document).click(function(){
        $("#add_report").fadeOut('slow');
    });
    //treeview add chart
    $(".addchart").click(function(e){
        $('#add_chart').slideToggle();
        e.stopPropagation();
    });
    $("#add_chart").click(function(e){
    e.stopPropagation();
    });

    $(document).click(function(){
        $("#add_chart").fadeOut('slow');
    });

    //add scroll-thin for treeview add-report
    $(".add_scroll_report").mouseover(function(){
        $(".add_scroll_report").addClass("scroll-thin");
    });
    $(".add_scroll_report").mouseout(function(){
        $(".add_scroll_report").removeClass("scroll-thin");
    });
    // add scroll for treeview add chart
    $(".add_scroll_chart").mouseover(function(){
        $(".add_scroll_chart").addClass("scroll-thin");
    });
    $(".add_scroll_chart").mouseout(function(){
        $(".add_scroll_chart").removeClass("scroll-thin");
    });

    $(".add_scroll").mouseover(function(){
        $("#add_scroll").attr('class',"scroll-thin");
    });
    $(".add_scroll").mouseout(function(){
        $("#add_scroll").attr('class',"scroll-thin2");
    });
});
//

// $(document).ready(function(){
//     $(".carte_button1").mouseover(function(){
//         $("#detail_report").show();
//         $("#img").hide();
//     });
//     $(".carte_button1").mouseout(function(){
//         $("#detail_report").hide('fast');
//         $("#img").show();

//     });
// });
// //hide scroll-thin

//tree view
$.fn.extend({
    treed: function (o) {
      
      var openedClass = 'fa-minus-square-o';
      var closedClass = 'fa-plus-square-o';
      
      if (typeof o != 'undefined'){
        if (typeof o.openedClass != 'undefined'){
        openedClass = o.openedClass;
        }
        if (typeof o.closedClass != 'undefined'){
        closedClass = o.closedClass;
        }
      };
      
        //initialize each of the top levels
        var tree = $(this);
        tree.addClass("tree");
        tree.find('li').has("ul").each(function () {
            var branch = $(this); //li with children ul
            branch.prepend("<i class='indicator fa " + closedClass + "'></i>");
            branch.addClass('branch');
            branch.on('click', function (e) {
                if (this == e.target) {
                    var icon = $(this).children('i:first');
                    icon.toggleClass(openedClass + " " + closedClass);
                    $(this).children().children().slideToggle('slow');
                }
            })
            branch.children().children().toggle();
        });
        //fire event from the dynamically added icon
      tree.find('.branch .indicator').each(function(){
        $(this).on('click', function () {
            $(this).closest('li').click();
        });
      });
        //fire event to open branch if the li contains an anchor instead of text
        tree.find('.branch>a').each(function () {
            $(this).on('click', function (e) {
                $(this).closest('li').click();
                e.preventDefault();
            });
        });
        //fire event to open branch if the li contains a button instead of text
        tree.find('.branch>button').each(function () {
            $(this).on('click', function (e) {
                $(this).closest('li').click();
                e.preventDefault();
            });
        });
    }

});

//Initialization of treeviews

$('#tree1').treed();
$('#tree2').treed();

