$(document).ready(function(){
	$(".box-chart1").mouseover(function(){
		$(".delete-combo-chart a").removeClass("d-none");
		$("#delete1").addClass("delete_combo_chart1");
		$("#delete1").click(function(){
			$("#box1").removeClass("box-chart1");
        	$("#box1").addClass("box-chart11");
		});
	});
	$(".box-chart1").mouseout(function(){
		$(".delete-combo-chart a").addClass("d-none");
		$("#delete1").removeClass("delete_combo_chart1");
		
	});
	$(".box-chart2").mouseover(function(){
		$(".delete-chart-div a").removeClass("d-none");
		$("#delete2").addClass("delete_chart_div1");
		$("#delete2").click(function(){
			$("#box2").removeClass("box-chart2");
        	$("#box2").addClass("box-chart22");
		});
	});
	$(".box-chart2").mouseout(function(){
		$(".delete-chart-div a").addClass("d-none");
		$("#delete2").removeClass("delete_chart_div1");
	});
	$(".box-chart3").mouseover(function(){
		$(".delete-curve-chart a").removeClass("d-none");
		$("#delete3").addClass("delete_curve_chart1");
		$("#delete3").click(function(){
			$("#box3").removeClass("box-chart3");
        	$("#box3").addClass("box-chart33");
		});
	});
	$(".box-chart3").mouseout(function(){
		$(".delete-curve-chart a").addClass("d-none");
		$("#delete3").removeClass("delete_curve_chart1");
	});
	$(".box-chart4").mouseover(function(){
		$(".delete-donut-single a").removeClass("d-none");
		$("#delete4").addClass("delete_curve_chart1");
		$("#delete4").click(function(){
			$("#box4").removeClass("box-chart4");
        	$("#box4").addClass("box-chart44");
		});
	});
	$(".box-chart4").mouseout(function(){
		$(".delete-donut-single a").addClass("d-none");
		$("#delete4").removeClass("delete_curve_chart1");
	});
});