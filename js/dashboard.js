google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawChart);
function drawChart() {
  // pie chart
  var data = google.visualization.arrayToDataTable([
    ['Effort', 'Amount given',],
    ['luong phoi ban ra',     70],
    ['luong thep ban ra',     30]
  ]);

  var options = {
    title: "Bao cao doanh thu theo mac thep",
    pieHole: 0.5,
    pieSliceTextStyle: {
      color: 'white',
    },
    legend: {position: 'right', textStyle: {color: 'black', fontSize: 11},position: 'right', alignment: 'center'},
    theme: {chartArea: {width: '70%', height: '70%'}},
    width: 350,
    height: 250
    // height: ,250width: 400

  };
  var chart = new google.visualization.PieChart(document.getElementById('donut_single'));
  chart.draw(data, options);
// area-chart

  var data = google.visualization.arrayToDataTable([
    [ 'Year', 'Sales', 'Expenses'],
    [ '',  1000, 400],
    [ '',  1170, 460],
    [ '',  660,  1120],
    [ '',  1030, 540],
    [ '',  1000, 400],
    [ '',  1000, 400],
    [ '',  1000, 400],
    [ '',  1000, 400],
    [ '',  1000, 400],
    [ '',  1000, 400],
    [ '',  1000, 400],
    [ '',  1000, 400],
  ]);

  var options = {
    title: 'BAO CAO TI TRONG SAN XUAT PHAN PHOI THEP',
    hAxis: {},
    vAxis: {minValue: 0,ticks:[]},
    legend :{position: 'bottom', textStyle: {color: 'blue', fontSize: 16}},
    // height: 500,
    width: 350,
    height: 250
  };
  var chart = new google.visualization.AreaChart(document.getElementById('chart_div'));
  chart.draw(data, options);
  //line-chart
  var data = google.visualization.arrayToDataTable([
    ['Year', 'Sales', 'Expenses'],
    ['',  1000,      400],
    ['',  1170,      460],
    ['',  660,       1120],
    ['',  1030,      540]
  ]);

  var options = {
    title: 'Company Performance',
    curveType: 'function',
    legend: { position: 'bottom' },
    // height: 400,
    width: 350,
    height: 250
  };

  var chart = new google.visualization.LineChart(document.getElementById('curve_chart'));

  chart.draw(data, options);
// Some raw data (not necessarily accurate)
  var data = google.visualization.arrayToDataTable([
     ['Month', 'Bolivia', 'Ecuador', 'Madagascar', 'Papua New Guinea', 'Rwanda', 'Average'],
     ['t1',  165,      938,         522,             998,           450,      614.6],
     ['t2',  135,      1120,        599,             1268,          288,      682],
     ['t3',  157,      1167,        587,             807,           397,      623],
     ['t4',  139,      1110,        615,             968,           215,      609.4],
     ['t5',  136,      691,         629,             1026,          366,      569.6]
  ]);

  var options = {
    title : 'Bao cao ban hang',
    seriesType: 'bars',
    series: {5: {type: 'line'}},
    // height: 300,
    width: 350,
    height: 250
  };

  var chart = new google.visualization.ComboChart(document.getElementById('combo_chart'));
  chart.draw(data, options);
  }