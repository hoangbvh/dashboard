(function($) {
    "use strict"; // Start of use strict
    $('.checkAll').on('click', function () {
    $(this).closest('table').find('.table-pannel tbody :checkbox')
      .prop('checked', this.checked)
      .closest('tr').toggleClass('selected', this.checked);
  });

  $('.table-pannel tbody :checkbox').on('click', function () {
    $(this).closest('tr').toggleClass('selected', this.checked); //Classe de seleção na row

    $(this).closest('table').find('.checkAll').prop('checked', ($(this).closest('table').find('tbody :checkbox:checked').length == $(this).closest('table').find('tbody :checkbox').length)); //Tira / coloca a seleção no .checkAll
  });

    // Show right bar
    $(".end-box").on('click', function(e) {
        e.preventDefault();
        // $(".left-content").hide("slow");
        var clicks = $(this).data('clicks');
        if (clicks) {
            $(".right-content").addClass('col-md-11-5 col-sm-11 col-xs-11');
            $(".right-content").addClass('fix-w');
            $(".left-content").removeClass("d-none");
            $(".fa-chevron-left").addClass('back');
            $(".end-box").addClass('show-box');
            $(".end-box").removeClass('hide-box');
            $('.right-content').css('width', width_screen-45);
            $('.right-content').attr('data-full','none');
        } else {
            $(".end-box").removeClass('show-box');
            $(".end-box").addClass('hide-box');
            $(".right-content").removeClass('col-md-11-5 col-sm-11 col-xs-11');
            $(".right-content").removeClass('fix-w');
            if ($(".hide-box")) {
                $(".hide-box").mouseover(function(e) {
                    e.preventDefault();
                    $(".hide-box").css('right', '0px');
                }).mouseout(function(e) {
                    e.preventDefault();
                    $(".hide-box").css('right', '-20px');
                });
            }
            $('.show_task').addClass('opentask').hide();
            $('.show_task').attr('data-show','show');
            $(".left-content").addClass("d-none");
            $(".fa-chevron-left").removeClass('back');
            $('.right-content').css('width', width_screen+15);
            $('.right-content').attr('data-full','full');
        }
        $(this).data("clicks", !clicks);
    });

    // Click show task right bar
    $('.click_open_task').click(function(e) {
        var state = $('.show_task').attr('data-show');
        e.stopPropagation();
        if (state == 'show') {
          $('.show_task').addClass('opentask').show();
          $('.show_task').attr('data-show','hide');
          $('.click_open_task .task').addClass('right-active');
          $('.click_open_task .task .badge').addClass('d-none');
        }else{
          $('.show_task').addClass('opentask').hide();
          $('.show_task').attr('data-show','show');
          $('.click_open_task .task').removeClass('right-active');
          $('.click_open_task .task .badge').removeClass('d-none');
        }
    });

    $(document).click(function(e) {
      var temp = $(e.target).parents().filter(function(index,value) {
        return $(value).attr('class') == 'box_show_task';
      });
      if (temp.length == 0) {
        $('.show_task').addClass('opentask').hide();
        $('.show_task').attr('data-show','show');
        $('.click_open_task .task').removeClass('right-active');
        $('.click_open_task .task .badge').removeClass('d-none');
      }else {

      }
    });

    $('.title-panel').click(function() {
        var clicks = $(this).data('clicks');
        if (clicks) {
            $('.box-panel-blur').addClass('d-none');
        } else {
            $('.box-panel-blur').removeClass('d-none');
        }
        $(this).data("clicks", !clicks);
    });

    // Fixed height screen
    var height_screen = $(window).height();
    var width_screen = $('.contai-full' ).width();
    $('.sidebar-left').css('height', height_screen);
    $('.right-content').css('height', height_screen - 72);
    $('.right-content').css('width', width_screen-45);
    $('.left-content').css('height', height_screen - 72);
    console.log(width_screen);
    // Moseover and Moseout Box-message
    $(".box-message").mouseover(function(e) {
        e.preventDefault();
        $(".group-mess").addClass('scroll-thin');
    }).mouseout(function(e) {
        e.preventDefault();
        $(".group-mess").removeClass('scroll-thin');
    });

    $( window ).resize(function() {
      var width_screen = $('.contai-full' ).width();
      var data_full = $('.right-content').attr('data-full');
      if (data_full == 'full') {
        $('.right-content').css('width', width_screen+15);
      }else {
        $('.right-content').css('width', width_screen-45);
      }
    });
    // Click Show change tabs
    $('.view-dashboard').on('click', function() {
        $('a[href="#dashboard"]').tab('show');
    });
    $('.view-doc').on('click', function() {
        $('a[href="#doc"]').tab('show');
    });
    $('.view-quytrinh').on('click', function() {
        $('a[href="#quytrinh"]').tab('show');
    });
    $('.view-report').on('click', function() {
        $('a[href="#report"]').tab('show');
    });
    $('.view-task').on('click', function() {
        $('a[href="#task"]').tab('show');
    });
    $('.view-calendar').on('click', function() {
        $('a[href="#calendar"]').tab('show');
    });
    $('.view-mail').on('click', function() {
        $('a[href="#mail"]').tab('show');
    });
    $('.view-user').on('click', function() {
        $('a[href="#user"]').tab('show');
    });

    // Change view table or list document

    $('.fc-table').click(function() {
      $('.view-table').removeClass('d-none');
      $('.view-list').addClass('d-none');
      $('.fc-table').addClass('view-active');
      $('.fc-list').removeClass('view-active');
    });

    $('.fc-list').click(function() {
      $('.view-table').addClass('d-none');
      $('.view-list').removeClass('d-none');
      $('.fc-table').removeClass('view-active');
      $('.fc-list').addClass('view-active');
    });

    // Event Click Infor Document

    $('.fc-infor').click(function() {
        var clicks = $(this).data('clicks');
        if (clicks) {
            $('.show-infor').addClass('d-none');
        } else {
            $('.show-infor').removeClass('d-none');
        }
        $(this).data("clicks", !clicks);
    });
    $(document).click(function(e) {
        $('.fc-r-click').addClass('d-none');
    });

    var handler = function(e) {
        e.preventDefault();
        console.log(e.button);
        if (e.button == 2) {
            $('.fc-r-click').removeClass('d-none');
        }
    }
    $(document).click(function() {
        $('.fc-r-click').addClass('d-none');
    });
    $('.doc1-box').on('contextmenu', handler);
    // $('.doc1-box').on('click', handler);

    $(".dp-inline-block").mouseover(function(e) {
        e.preventDefault();
        var taskid = this.id;
        $('#' + taskid).addClass('hover-task');
        $('#' + taskid + ' .fl-r').removeClass('d-none');
    }).mouseout(function(e) {
        e.preventDefault();
        var taskid = this.id;
        $('#' + taskid).removeClass('hover-task');
        $('#' + taskid + ' .fl-r').addClass('d-none');
    });

    // Open toggle sidebar left

    $("#open-toggle").on('click', function(e) {
        e.preventDefault();
        var clicks = $(this).data('clicks');
        if (clicks) {
            $(".sidebar").addClass("sidebar-po");
            $(".unpin").addClass("d-none");
            $(".pin").removeClass("d-none");
            $(".div-width-60").removeClass('d-none');
            $(".sidebar").mouseover(function(e) {
                e.preventDefault();
                $(".sidebar").addClass("sidebar-po");
                $(".div-width-60").removeClass('d-none');
                $("#open-toggle").removeClass('d-none');
                $(".sidebar").removeClass("toggled");
                $(".sidebar").addClass("min-w");

                $(".left-sidebar-title").removeClass('d-none');
                $(".card-body").removeClass("p-0");
                $(".title-small-sidebar").addClass("d-none");
                $(".sidebar-left-end").removeClass("d-none");

            }).mouseout(function(e) {
                e.preventDefault();
                $(".sidebar").removeClass("sidebar-po");
                $(".div-width-60").addClass('d-none');
                $("#open-toggle").addClass('d-none');
                $(".sidebar").removeClass("min-w");
                $(".left-sidebar-title").addClass('d-none');
                $(".title-small-sidebar").removeClass("d-none");
                $(".sidebar").addClass("toggled");
                $(".card-body").addClass("p-0");
                $(".sidebar-left-end").addClass("d-none");
            });
            $(".sidebar").addClass("toggled");
            $(".left-sidebar-title").addClass('d-none');
            $(".card-body").addClass("p-0");
        } else {
            $(".div-width-60").addClass('d-none');
            $(".sidebar").removeClass("sidebar-po");
            $('.sidebar').off('mouseover');
            $('.sidebar').off('mouseout');
            $(".sidebar").removeClass("toggled");
            $(".left-sidebar-title").removeClass('d-none');
            $(".card-body").removeClass("p-0");
            $(".unpin").removeClass("d-none");
            $(".pin").addClass("d-none");
        }
        $(this).data("clicks", !clicks);

    });

    // Mouseover and mouseout sidebar left
    $(".sidebar").mouseover(function(e) {
        e.preventDefault();
        $("#open-toggle").removeClass('d-none');
        $(this).animate(1500).removeClass('toggled').addClass('sidebar-po min-w');
        $(".div-width-60").removeClass('d-none');
        $(".left-sidebar-title").removeClass('d-none');
        $(".card-body").removeClass("p-0");
        $(".title-small-sidebar").addClass("d-none");
        $(".sidebar-left-end").removeClass("d-none");
    }).mouseout(function(e) {
        e.preventDefault();
        $("#open-toggle").addClass('d-none');
        $(".sidebar").removeClass("min-w",1000,"linear");
        $(".sidebar").removeClass("sidebar-po");
        $(".div-width-60").addClass('d-none');
        $(".left-sidebar-title").addClass('d-none');
        $(".title-small-sidebar").removeClass("d-none");
        $(".sidebar").addClass("toggled",1000,"linear");
        $(".card-body").addClass("p-0");
        $(".sidebar-left-end").addClass("d-none");

    });
    // Prevent the content wrapper from scrolling when the fixed side navigation hovered over
    $('body.fixed-nav .sidebar').on('mousewheel DOMMouseScroll wheel', function(e) {
        if ($(window).width() > 768) {
            var e0 = e.originalEvent,
                delta = e0.wheelDelta || -e0.detail;
            this.scrollTop += (delta < 0 ? 1 : -1) * 30;
            e.preventDefault();
        }
    });

    // Scroll to top button appear
    $(document).on('scroll', function() {
        var scrollDistance = $(this).scrollTop();
        if (scrollDistance > 100) {
            $('.scroll-to-top').fadeIn();
        } else {
            $('.scroll-to-top').fadeOut();
        }
    });

    // Smooth scrolling using jQuery easing
    $(document).on('click', 'a.scroll-to-top', function(event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: ($($anchor.attr('href')).offset().top)
        }, 1000, 'easeInOutExpo');
        event.preventDefault();
    });

})(jQuery); // End of use strict

// function show hidden pass
function show_new_pass(argument) {
    var x = document.getElementById("new-password");
    if (x.type === "password") {
        x.type = "text";
    } else {
        x.type = "password";
    }
}

function show_conf_pass(argument) {
    var x = document.getElementById("conf-new-pass");
    if (x.type === "password") {
        x.type = "text";
    } else {
        x.type = "password";
    }
}
